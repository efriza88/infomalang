import twitter
import pymongo as p 

client = p.MongoClient('localhost', 27017)

# Get the sampleDB database
db = client.infokost.tweets




#-----------------------------------------------------------------------

# create twitter API object

#-----------------------------------------------------------------------
ACCESS_TOKEN = 'YOURACCESTOKEN'
ACCESS_SECRET = 'YOURACESSECRET'
CONSUMER_KEY = 'YOURCONSUMERKEY'
CONSUMER_SECRET = 'YOURCONSUMERSECRET'

twitterapi = twitter.Twitter(auth = twitter.OAuth(ACCESS_TOKEN,ACCESS_SECRET,
                                                  CONSUMER_KEY,CONSUMER_SECRET))
#-----------------------------------------------------------------------
# this is the user we're going to query.
#-----------------------------------------------------------------------
user = "infokostmalang"
#-----------------------------------------------------------------------
# query the user timeline.
# twitter API docs:
# https://dev.twitter.com/rest/reference/get/statuses/user_timeline
#-----------------------------------------------------------------------
results = twitterapi.statuses.user_timeline(screen_name=user,count=50,tweet_mode='extended')
#-----------------------------------------------------------------------
# loop through each status item, and print its content.
#-----------------------------------------------------------------------
for status in results:
        print("%s %s %s %s" % (status["id"],status["created_at"], status["full_text"],status["in_reply_to_status_id"]))
        print('\n')
        docs = {"id" : status["id"],"created_at" : status["created_at"], "text" : status["full_text"],
        "in_reply":status["in_reply_to_status_id"]}
        db.insert_one(docs)
        


